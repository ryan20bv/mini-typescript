import React, { useState, useEffect, useCallback, useRef } from "react";

const slides = [
	{ id: "c1", color: "red", name: "first div" },
	{ id: "c2", color: "yellow", name: "second div" },
	{ id: "c3", color: "green", name: "third div" },
];
const MovieCarousel = () => {
	const [selected, setSelected] = useState(0);
	const timeOutRef = useRef(null);

	const addSelectedHandler = useCallback(() => {
		const numOfItem = slides.length;
		let newNum = selected + 1;
		if (newNum > numOfItem - 1) {
			newNum = 0;
		}
		setSelected(newNum);
	}, [selected]);
	const reduceSelectedHandler = () => {
		const numOfItem = slides.length;
		let newNum = selected - 1;
		if (newNum < 0) {
			newNum = numOfItem - 1;
		}
		setSelected(newNum);
	};
	function resetTimeout() {
		if (timeOutRef.current) {
			clearTimeout(timeOutRef.current);
		}
	}

	useEffect(() => {
		resetTimeout();
		const timeOut = setTimeout(addSelectedHandler, 2500);
		if (typeof timeOut === "number") {
			timeOutRef.current = timeOut;
		}
		return () => {
			resetTimeout();
		};
	}, [selected, addSelectedHandler]);

	const output = slides.map((item, index) => (
		<div
			className={` w-48 h-48 bg-${item.color}-400 duration-700 ease-in-out ${
				index === selected ? "flex" : "hidden"
			}`}
			key={item.id}
		>
			{item.name}
		</div>
	));

	const buttonOutput = slides.map((item, index) => (
		<button
			key={item.id}
			type='button'
			className={`w-3 h-3 rounded-full ${
				selected === index ? "bg-black" : "bg-white"
			} `}
			aria-current='true'
			aria-label='Slide 1'
			data-carousel-slide-to='0'
		></button>
	));

	return (
		<div>
			<div className='pt-10 border border-solid border-blue-500 flex flex-col'>
				<h1>Movie Carousel</h1>
				<div className='border border-solid border-red-400  w-max self-center flex'>
					<div className='text-2xl mx-8' onClick={reduceSelectedHandler}>
						{" "}
						{"<"}
					</div>
					<div>{output}</div>
					<div className='text-2xl mx-8' onClick={addSelectedHandler}>
						{" "}
						{">"}
					</div>
				</div>
				<div className='border border-solid border-black'>{buttonOutput}</div>
			</div>
		</div>
	);
};
export default MovieCarousel;
