import React, { useRef, useState } from "react";
import { NavLink } from "react-router-dom";

// import classNamees from "./styles/nav.module.css";
const activeNav =
	"block py-2 pl-3 pr-4 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white";
const unActiveNav =
	"block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700";

const Nav = () => {
	const [menuOpen, setMenuOpen] = useState<boolean>(false);
	const divUlRef = useRef<HTMLDivElement>(null);

	const showMenuHandler = () => {
		setMenuOpen((prevState) => !prevState);
		divUlRef.current!.classList.toggle("max-[768px]:hidden");
	};

	return (
		<nav className='bg-white fixed w-full z-20 top-0 left-0 border-b border-black h-10 md:h-14 mb-4'>
			<div
				className='flex justify-between'
				/* className='container flex flex-wrap items-center justify-between mx-auto' */
			>
				<div className='bg-black h-10 md:h-14  w-2/12 flex items-center justify-center rounded-tr-2xl text-white'>
					<a
						href='https://flowbite.com/'
						// className='flex items-center justify-center'
					>
						ZtZ
					</a>
				</div>
				<div className='w-8/12 bg-black md:relative h-10 md:h-14 md:hidden'>
					<div className='w-full h-full bg-white rounded-bl-2xl rounded-tr-2xl '></div>
				</div>

				<div className=' md:order-2 bg-white w-2/12 h-10 md:h-14 md:h-14'>
					<div className=' bg-black w-full rounded-bl-2xl flex items-center justify-center h-10 md:h-14'>
						<button
							data-collapse-toggle='navbar-sticky'
							type='button'
							className='inline-flex items-center p-2 text-sm text-white rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2  '
							aria-controls='navbar-sticky'
							aria-expanded='false'
							onClick={showMenuHandler}
						>
							<span className='sr-only'>Open main menu</span>
							{!menuOpen && (
								<svg
									xmlns='http://www.w3.org/2000/svg'
									fill='none'
									viewBox='0 0 24 24'
									strokeWidth={1.5}
									stroke='currentColor'
									className='w-6 h-6 font-black border border-solid'
								>
									<path
										strokeLinecap='round'
										strokeLinejoin='round'
										d='M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5'
										className='font-black 10'
									/>
								</svg>
							)}
							{menuOpen && (
								<svg
									xmlns='http://www.w3.org/2000/svg'
									fill='none'
									viewBox='0 0 24 24'
									strokeWidth={1.5}
									stroke='currentColor'
									className='w-6 h-6 text-red-600 text-white'
								>
									<path
										strokeLinecap='round'
										strokeLinejoin='round'
										d='M6 18L18 6M6 6l12 12'
									/>
								</svg>
							)}
						</button>
					</div>
				</div>
				<div
					className='absolute max-[768px]:hidden top-10 max-[768px]:right-0 w-full md:w-8/12 md:h-14 md:static bg-black'
					id='navbar-sticky'
					ref={divUlRef}
				>
					<ul
						className=' p-2 font-normal border border-gray-100 md:h-14 bg-white  md:space-x-8 md:mt-0 md:text-lg md:font-medium md:border-0 md:bg-bg-white md:rounded-bl-2xl md:rounded-tr-2xl
					md:flex md:flex-row md:items-center md:justify-center '
					>
						<NavLink
							to='/home'
							className={(navData) =>
								navData.isActive ? activeNav : unActiveNav
							}
							aria-current='page'
							onClick={showMenuHandler}
						>
							Home
						</NavLink>
						<NavLink
							to='/bill-splitter'
							className={(navData) =>
								navData.isActive ? activeNav : unActiveNav
							}
							onClick={showMenuHandler}
						>
							Bill Splitter
						</NavLink>
						<NavLink
							to='/number-to-word'
							className={(navData) =>
								navData.isActive ? activeNav : unActiveNav
							}
							onClick={showMenuHandler}
						>
							NumToWord
						</NavLink>
						<NavLink
							to='/movie'
							className={(navData) =>
								navData.isActive ? activeNav : unActiveNav
							}
							onClick={showMenuHandler}
						>
							Movie
						</NavLink>
					</ul>
				</div>
			</div>
		</nav>
	);
};

export default Nav;
