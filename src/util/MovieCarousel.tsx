import React, { useState, useEffect, useCallback, useRef } from "react";
import { MovieModel } from "../components/pages/movies/model/MovieModel";

interface PropsType {
	initialMovie: MovieModel[];
	onOpenModal: (id: string) => void;
}

const MovieCarousel: React.FC<PropsType> = (props) => {
	const timeOutRef = useRef(null);
	const [shownItem, setShownItem] = useState<number>(0);
	const origLength: number = props.initialMovie.length;

	const upChangeHandler = useCallback(() => {
		let newNum = shownItem + 1;
		if (newNum > origLength - 1) {
			newNum = 0;
		}
		setShownItem(newNum);
	}, [shownItem, origLength]);
	const downChangeHandler = () => {
		let newNum = shownItem - 1;
		if (newNum < 0) {
			newNum = origLength - 1;
		}
		setShownItem(newNum);
	};

	function resetTimeout() {
		if (timeOutRef.current) {
			clearTimeout(timeOutRef.current);
		}
	}

	useEffect(() => {
		resetTimeout();
		const timeOut = setTimeout(upChangeHandler, 2500);
		if (typeof timeOut === "number") {
			timeOutRef.current = timeOut;
		}
		return () => {
			resetTimeout();
		};
	}, [shownItem, upChangeHandler]);

	const outPut = props.initialMovie.map((item, index) => (
		<div
			className={`w-full h-full ${
				index === shownItem ? "flex" : "hidden"
			} relative duration-700 ease-in-out lg:static `}
			key={item.id}
			onClick={props.onOpenModal.bind(null, item.id)}
		>
			<h1 className='absolute text-xl px-6 lg:static lg:w-4/12 lg:h-full lg:flex lg:items-center lg:justify-center'>
				{item.title}
			</h1>
			<img
				className='w-full h-full lg:w-8/12 hover:cursor-pointer'
				src={item.thumbnail}
				alt={item.title}
			/>
		</div>
	));

	const buttonOutput = props.initialMovie.map((item, index) => (
		<button
			key={item.id}
			type='button'
			className={`w-3 h-3 rounded-full mx-1 ${
				shownItem === index ? "bg-red-600" : "bg-white"
			} `}
			aria-current='true'
			aria-label='Slide 1'
			data-carousel-slide-to='0'
		></button>
	));
	return (
		<div className='h-64 flex items-center mb-8 lg:px-14 lg:mt-4'>
			<div
				className='hover:cursor-pointer w-1/12 flex text-5xl place-content-center absolute z-10 left-2'
				onClick={downChangeHandler}
			>
				{"<"}
			</div>
			<div className='w-full  h-full flex flex-col place-content-center items-center relative'>
				{outPut}
				<div className='absolute bottom-0'>{buttonOutput}</div>
			</div>
			<div
				className='hover:cursor-pointer w-1/12 flex text-5xl place-content-center absolute z-10 right-2'
				onClick={upChangeHandler}
			>
				{">"}
			</div>
		</div>
	);
};

export default MovieCarousel;
