import React from "react";

interface PropsType {
	id: string;
	label: string;
	unit: number;
}

const ListUtility: React.FC<PropsType> = (props) => {
	return (
		<li className='py-3 sm:py-4' key={props.id}>
			<div className='flex items-center space-x-4'>
				<div className='flex-1 min-w-0'>
					<p className='text-sm font-medium text-gray-900 truncate dark:text-white'>
						{props.label}
					</p>
				</div>
				<div className='inline-flex items-center text-base font-semibold text-gray-900 dark:text-white'>
					{props.unit}
				</div>
			</div>
		</li>
	);
};

export default ListUtility;
