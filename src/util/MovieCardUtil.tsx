import React from "react";

interface PropsType {
	embed_url: string;
	id: string;
	thumbnail: string;
	title: string;
	youtube_id: string;
	onOpenModal: () => void;
}

const MovieCardUtil: React.FC<PropsType> = (props) => {
	return (
		<div className='bg-white  rounded-lg shadow-md mt-2 mb-4 mx-2 w-48 h-56'>
			<img
				className='rounded-t-lg w-48 h-48'
				src={props.thumbnail}
				alt={props.title}
			/>

			<div className='px-2 py-1 rounded-b-lg flex hover:cursor-pointer'>
				<h5
					className='text-base font-bold tracking-tight text-gray-900 '
					onClick={props.onOpenModal}
				>
					{props.title.substring(0, 18)}
					{props.title.length > 18 && "..."}
				</h5>
			</div>
		</div>
	);
};

export default MovieCardUtil;
