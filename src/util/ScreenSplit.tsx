import React from "react";

// import classes from "./styles/screenSplit.module.css";

interface PropsType {
	children: React.ReactNode[];
}
const ScreenSplit: React.FC<PropsType> = (props) => {
	return (
		<section className='divide-solid border-red-600 border grid grid-cols-2 divide-x'>
			<div>{props.children[0]}</div>
			<div>{props.children[1]}</div>
		</section>
	);
};

export default ScreenSplit;
