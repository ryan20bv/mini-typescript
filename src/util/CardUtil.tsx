import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

interface PropsType {
	title: string;
	description: string;
	imageUrl: string[];
	link: string;
}

const CardUtility: React.FC<PropsType> = (props) => {
	const navigate = useNavigate();
	const [selectedImage, setSelectedImage] = useState<number>(0);
	const imageOutput = props.imageUrl.map((item, index) => {
		return (
			<img
				className={`rounded-t-lg h-56 md:w-64 ${
					selectedImage === index ? "block" : "hidden"
				} `}
				src={require(`../images/${item}`)}
				alt=''
				key={index}
			/>
		);
	});

	const clickRightHandler = () => {
		const imageUrlLength = props.imageUrl.length;
		let newSelect = selectedImage + 1;
		if (newSelect > imageUrlLength - 1) {
			newSelect = 0;
		}
		setSelectedImage(newSelect);
	};
	const clickLeftHandler = () => {
		const imageUrlLength = props.imageUrl.length;
		let newSelect = selectedImage - 1;
		if (newSelect < 0) {
			newSelect = imageUrlLength - 1;
		}
		setSelectedImage(newSelect);
	};

	const navigateHandler = () => {
		navigate(`${props.link}`);
	};
	return (
		<div className='max-w-sm bg-white border border-gray-200 rounded-lg shadow-md  mx-8 md:flex md:w-screen mb-4 lg:h-64'>
			<div className='relative flex items-center justify-content-center md:w-screen '>
				<div
					className='absolute h-full text-4xl  flex items-center text-red-500 hover:cursor-pointer'
					onClick={clickLeftHandler}
				>
					<h1>{"<"}</h1>
				</div>
				{imageOutput}
				<div
					className='absolute h-full text-4xl  flex items-center right-0 text-red-500 hover:cursor-pointer'
					onClick={clickRightHandler}
				>
					<h1>{">"}</h1>
				</div>
			</div>

			<div
				className='p-5 bg-gray-200 rounded-b-lg hover:cursor-pointer'
				onClick={navigateHandler}
			>
				<h5 className='mb-2 text-2xl font-bold tracking-tight text-gray-900 '>
					{props.title}
				</h5>

				<p className='mb-3 font-normal text-gray-700 dark:text-gray-400'>
					{props.description}
				</p>
			</div>
		</div>
	);
};
export default CardUtility;
