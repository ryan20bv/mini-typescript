import React from "react";

interface PropsType {
	nameId: string;
	type?: string;
	reference?: React.RefObject<HTMLInputElement>;
	min?: string;
	label?: string;
	require?: boolean;
}

const InputUtility: React.FC<PropsType> = (props) => {
	return (
		<div className='mb-4'>
			<label
				htmlFor={props.nameId}
				className='block mb-2 text-sm font-medium text-gray-900 dark:text-white'
			>
				{props.label}
			</label>
			<input
				type={props.type}
				id={props.nameId}
				className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 w-full py-2 px-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
				// placeholder='John'
				ref={props.reference}
				min={props.min}
				required={props.require}
			/>
		</div>
	);
};

export default InputUtility;
