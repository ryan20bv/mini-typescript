import React from "react";

const LoadingUtil = () => {
	return (
		<div className='bg-black text-white w-screen h-screen flex items-center justify-center text-xl'>
			<h1>Loading...</h1>
		</div>
	);
};
export default LoadingUtil;
