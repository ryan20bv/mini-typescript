import React from "react";

import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import MainNav from "./components/pages/MainNav";
import Home from "./components/pages/Home";

import BillSplitter from "./components/pages/BillSplitter";
import BasicSplitter from "./components/pages/billSplitter/basic/BasicSplitter";
import AdvanceSplitter from "./components/pages/billSplitter/advance/AdvanceSplitter";
import NumToWord from "./components/pages/NumToWord";
import Movie from "./components/pages/Movie";
import MovieCarousel from "./util/Carousel";
import LoadingUtil from "./util/Loading";

function App() {
	return (
		<BrowserRouter>
			<MainNav />
			<Routes>
				<Route path='*' element={<Navigate to='/home' />} />
				<Route path='/home' element={<Home />} />
				<Route path='/bill-splitter' element={<BillSplitter />}>
					<Route path='basic' element={<BasicSplitter />} />
					<Route path='advance' element={<AdvanceSplitter />} />
				</Route>
				<Route path='/number-to-word' element={<NumToWord />} />
				<Route path='/movie' element={<Movie />} />
			</Routes>
		</BrowserRouter>
	);
}

export default App;
