import React from "react";
import ReactDOM from "react-dom";

const Backdrop: React.FC<{ onClose: () => void }> = (props) => {
	return (
		<div
			className='fixed z-20 top-0 left-0 w-full h-screen bg-gray-300  bg-opacity-75'
			onClick={props.onClose}
		/>
	);
};

const ModalOverlay: React.FC<{ children: React.ReactNode }> = (props) => {
	return (
		<div className='fixed top-36 w-10/12 h-3/6 left-1/2 z-30  bg-white -translate-x-2/4 shadow-md  text-center max-w-2xl'>
			{props.children}
		</div>
	);
};

const portalElement = document.getElementById("backdrop-root")! as HTMLElement;
const Modal: React.FC<{ onClose: () => void; children: React.ReactNode }> = (
	props
) => {
	return (
		<React.Fragment>
			{ReactDOM.createPortal(
				<Backdrop onClose={props.onClose} />,
				portalElement
			)}
			{ReactDOM.createPortal(
				<ModalOverlay>{props.children}</ModalOverlay>,
				portalElement
			)}
		</React.Fragment>
	);
};

export default Modal;
