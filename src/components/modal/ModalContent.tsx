import React from "react";

import { MovieModel } from "../pages/movies/model/MovieModel";

interface PropsType {
	movie: MovieModel;
}

const ModalContent: React.FC<PropsType> = (props) => {
	return (
		<div className='border-red-500 h-full'>
			<iframe
				className='w-full h-full'
				title={props.movie.title}
				src={props.movie.embed_url}
				allow='autoplay'
			>
				{/* Your browser does not support the video tag. */}
			</iframe>
		</div>
	);
};

export default ModalContent;
