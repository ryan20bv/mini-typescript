import React from "react";
import useNum from "./numToWord/hook/use-num";
import NumForm from "./numToWord/NumForm";

const NumToWord = () => {
	const [convertedWord, stringNum, convertHandler] = useNum();

	return (
		<div className='pt-20 flex flex-col items-center w-screen '>
			<h1 className='text-4xl font-medium mb-5'>Number to Word</h1>

			<NumForm convertHandler={convertHandler} />
			<div className='flex flex-col w-10/12 items-center font-medium text-2xl  py-4'>
				<h1 className='p-4'>{stringNum}</h1>
				<h1>{convertedWord}</h1>
			</div>
		</div>
	);
};

export default NumToWord;
