import React, { useState } from "react";

const NumForm: React.FC<{ convertHandler: (input: string) => void }> = ({
	convertHandler,
}) => {
	const [inputValue, setInputValue] = useState<string>("");

	const inputHandler = (e: React.FormEvent<HTMLInputElement>) => {
		const enteredNum = e.currentTarget.value;
		if (enteredNum.trim().length > 24) {
			return;
		}
		if (enteredNum.trim().length === 1) {
			if (+enteredNum === 0) {
				return;
			}
		}
		const numRegex: RegExp = /^[0-9\b]+$/;
		const checkedNum = numRegex.test(enteredNum);

		if (!checkedNum) {
			if (enteredNum.trim().length < 1) {
				setInputValue("");
			}
			return;
		}
		setInputValue(enteredNum);
	};

	const submitHandler = (e: React.FormEvent) => {
		e.preventDefault();
		convertHandler(inputValue);
	};
	return (
		<form
			onSubmit={submitHandler}
			className='flex flex-col w-screen items-center'
		>
			<input
				type='text'
				value={inputValue}
				onChange={inputHandler}
				className='w-10/12 sm:w-6/12 bg-gray-50 border border-gray-300 text-gray-900 text-xl rounded-lg focus:ring-blue-500 focus:border-blue-500 w-full py-2 px-2.5 text-center'
			/>
			<button
				type='submit'
				className='focus:outline-none text-white bg-green-500 hover:bg-green-400 w-4/12 m-5 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-green-600'
			>
				Convert
			</button>
		</form>
	);
};

export default NumForm;
