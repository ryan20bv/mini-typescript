import React from "react";
import CardUtility from "../../util/CardUtil";

const ProjectList: {
	id: string;
	title: string;
	description: string;
	imageUrl: string[];
	link: string;
}[] = [
	{
		id: "p1",
		title: "Bill Splitter",
		description: "An app that let you split the bill among others",
		imageUrl: ["split-1.PNG", "split-2.PNG", "split-3.PNG"],
		link: "/bill-splitter",
	},
	{
		id: "p2",
		title: "Number to Words",
		description: "Converts numbers to Words",
		imageUrl: ["num-1.PNG"],
		link: "/number-to-word",
	},
	{
		id: "p3",
		title: "Movie",
		description: "Converts numbers to Words",
		imageUrl: ["movie1.PNG", "movie2.PNG"],
		link: "/movie",
	},
];

const Home = () => {
	return (
		<div className='pt-20 flex flex-col items-center justify-center h-100%  lg:flex-row lg:flex-wrap lg:h-screen'>
			{ProjectList.map((item) => (
				<CardUtility
					key={item.id}
					title={item.title}
					description={item.description}
					imageUrl={item.imageUrl}
					link={item.link}
				/>
			))}
			;
		</div>
	);
};

export default Home;
