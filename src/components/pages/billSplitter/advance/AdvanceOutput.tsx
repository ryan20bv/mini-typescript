import React from "react";
import ListUtility from "../../../../util/ListUtil";
import PillUtility from "../../../../util/PillUtil";

class PersonShareType {
	id: string;
	personName: string;
	amountShare: number;
	constructor(person: string, amount: number) {
		this.id = Math.random().toString();
		this.personName = person;
		this.amountShare = +amount;
	}
}

interface PropsType {
	personShare: PersonShareType[];
	totalBill: string;
	totalShare: number;
	chippedAmount: string;
	balance: number;
	calculated: boolean;
	onDelete: (id: string) => void;
	onReset: () => void;
}

const AdvanceOutput: React.FC<PropsType> = (props) => {
	return (
		<div className=' p-8 max-w-md w-full bg-green-300 border rounded-lg shadow-md sm:h-full lg:w-9/12 sm:mb-3'>
			{!props.calculated && (
				<div className='text-2xl  flex flex-col items-center justify-center sm:h-full'>
					<p>Spending is Better</p>
					<p>If</p>
					<p>We spend Together!</p>
				</div>
			)}
			{props.personShare.length > 0 && (
				<div className='grid grid-cols-2 gap-3 mb-10'>
					{props.personShare.map((item) => (
						// <li key={item.id}>
						// 	{item.personName}: {item.amountShare}
						// </li>
						<PillUtility
							key={item.id}
							id={item.id}
							label={item.personName + " :"}
							unit={+item.amountShare}
							onDelete={props.onDelete.bind(null, item.id)}
						/>
					))}
				</div>
			)}
			<div className='flow-root'>
				<ul className='divide-y divide-gray-200 '>
					{props.calculated && (
						<>
							<ListUtility
								id={"Total Share"}
								label={"Total Share :"}
								unit={+props.totalShare}
							/>
							<ListUtility
								id={"Total Chipped-in"}
								label={"Total Chipped-in :"}
								unit={+props.chippedAmount}
							/>
							<ListUtility
								id={"Total Bill"}
								label={"Total Bill :"}
								unit={+props.totalBill}
							/>
							{props.balance >= 0 && (
								<ListUtility
									id={"Change is"}
									label={"Change is :"}
									unit={+props.balance}
								/>
							)}
							{props.balance < 0 && (
								<ListUtility
									id={"Fund needed is"}
									label={"Fund needed is :"}
									unit={Math.abs(props.balance)}
								/>
							)}
						</>
					)}
				</ul>
			</div>
			{props.calculated && (
				<button
					onClick={props.onReset}
					className='focus:outline-none w-6/12 text-white bg-red-500 hover:bg-red-400 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-4 py-2 self-center'
				>
					Reset
				</button>
			)}
		</div>
	);
};

export default AdvanceOutput;
