import React, { useState } from "react";
import ScreenSplit from "../../../../util/ScreenSplit";
import AdvanceForm from "./AdvanceForm";
import AdvanceOutput from "./AdvanceOutput";

class PersonShareType {
	id: string;
	personName: string;
	amountShare: number;
	constructor(person: string, amount: number) {
		this.id = Math.random().toString();
		this.personName = person;
		this.amountShare = +amount;
	}
}

const AdvanceSplitter: React.FC = () => {
	const [personShare, setPersonShare] = useState<PersonShareType[]>([]);
	const [totalBill, setTotalBill] = useState<string>("");
	const [chippedAmount, setChippedAmount] = useState<string>("");
	const [balance, setBalance] = useState<number>(0);
	const [totalShare, setTotalShare] = useState<number>(0);
	const [calculated, setCalculated] = useState<boolean>(false);

	const addPersonHandler = (name: string, amount: number) => {
		const totalPersonalShare = reducePersonalShareHandler();
		const newPersonalShare = totalPersonalShare + amount;
		balanceCalculateHandler(newPersonalShare, +chippedAmount, +totalBill);
		setTotalShare((prevShare) => prevShare + amount);
		setPersonShare((prevState) => {
			return prevState.concat(new PersonShareType(name, amount));
		});
	};
	const calculateHandler = (bill: string, chipped: string) => {
		setTotalBill(bill);
		setChippedAmount(chipped);
		const totalPersonalShare = reducePersonalShareHandler();
		balanceCalculateHandler(totalPersonalShare, +chipped, +bill);
		setCalculated(true);
	};

	const balanceCalculateHandler = (
		personalShare: number,
		chipped: number,
		bill: number
	) => {
		const remaining = personalShare + chipped - bill;
		setBalance(remaining);
	};
	const reducePersonalShareHandler = (): number => {
		const amountToReduce = personShare.map((item) => item.amountShare);
		const totalPersonalShare = amountToReduce.reduce(
			(accumulator, current) => accumulator + current,
			0
		);
		setTotalShare(totalPersonalShare);
		return totalPersonalShare;
	};
	const deleteShareHandler = (id: string) => {
		const personIndex = personShare.findIndex((indiv) => {
			return indiv.id === id;
		});

		const share = personShare[personIndex].amountShare;
		setBalance((prevBalance) => {
			return prevBalance - share;
		});
		setTotalShare((prevShare) => prevShare - share);
		setPersonShare((prevState) => {
			return prevState.filter((person) => person.id !== id);
		});
	};

	const resetHandler = () => {
		setPersonShare([]);
		setTotalBill("");
		setChippedAmount("");
		setBalance(0);
		setTotalShare(0);
	};

	return (
		<div className='mx-4 flex flex-col items-center mt-2 sm:grid sm:grid-cols-2 sm:gap-8 sm:mt-4 md:-mt-3 lg:gap-16'>
			<AdvanceForm
				onAddPerson={addPersonHandler}
				onCalculate={calculateHandler}
			/>
			<AdvanceOutput
				personShare={personShare}
				totalShare={totalShare}
				totalBill={totalBill}
				chippedAmount={chippedAmount}
				balance={balance}
				calculated={calculated}
				onDelete={deleteShareHandler}
				onReset={resetHandler}
			/>
		</div>
	);
};

export default AdvanceSplitter;
