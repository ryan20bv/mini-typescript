import React, { useRef } from "react";

import InputUtility from "../../../../util/InputUtil";

interface PropsType {
	onAddPerson: (name: string, amount: number) => void;
	onCalculate: (bill: string, chipped: string) => void;
}

const AdvanceForm: React.FC<PropsType> = (props) => {
	const nameInputRef = useRef<HTMLInputElement>(null);
	const amountShareInputRef = useRef<HTMLInputElement>(null);
	const billInputRef = useRef<HTMLInputElement>(null);
	const chippedInputRef = useRef<HTMLInputElement>(null);

	const clickHandler = (e: React.FormEvent) => {
		e.preventDefault();

		const nameInput = nameInputRef.current!.value;
		const amountShareInput = +amountShareInputRef.current!.value;

		if (
			nameInput.trim().length === 0 ||
			amountShareInput < 0 ||
			!amountShareInput
		) {
			return;
		}
		props.onAddPerson(nameInput, amountShareInput);
		nameInputRef.current!.value = "";
		amountShareInputRef.current!.value = "";
	};

	const computeSplitHandler = (e: React.FormEvent) => {
		e.preventDefault();
		const totalBillInput = billInputRef.current!.value;
		const chippedInput = chippedInputRef.current!.value;

		if (totalBillInput.trim().length <= 0 || chippedInput.trim().length <= 0) {
			return;
		}

		props.onCalculate(totalBillInput, chippedInput);
		billInputRef.current!.value = "";
		chippedInputRef.current!.value = "";
	};

	return (
		<div className=' w-full  flex flex-col items-center sm:justify-content-end sm:items-end'>
			<form
				onSubmit={computeSplitHandler}
				className='w-full max-w-md bg-blue-300 border rounded-lg shadow-md p-4 mb-3'
			>
				<InputUtility
					nameId={"total-bill"}
					type={"number"}
					reference={billInputRef}
					min={"0"}
					label={"Total Bill:"}
					require={true}
				/>

				{/* <div>
				<label htmlFor='total-bill'>Total Bill:</label>
				<input type='number' id='total-bill' ref={billInputRef} min='1' />
			</div> */}
				<InputUtility
					nameId={"chipped-amount"}
					type={"number"}
					reference={chippedInputRef}
					min={"0"}
					label={"Chipped-in Amount:"}
				/>
				{/* <div>
				<label htmlFor='chipped-amount'>Chipped-in Amount:</label>
				<input
					type='number'
					id='chipped-amount'
					ref={chippedInputRef}
					min='0'
				/>
			</div> */}

				<button className='focus:outline-none text-white bg-green-500 hover:bg-green-400 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800'>
					Calculate Split Bill
				</button>
			</form>
			<form className='w-full max-w-md bg-blue-300 border rounded-lg shadow-md p-4 mb-3'>
				<InputUtility
					nameId={"share-name"}
					type={"text"}
					reference={nameInputRef}
					label={"Name of person:"}
					require={true}
				/>
				{/* <div>
				<label htmlFor='share-name'>Name of person:</label>
				<input type='text' id='share-name' ref={nameInputRef} />
			</div> */}
				<InputUtility
					nameId={"share-amount"}
					type={"number"}
					reference={amountShareInputRef}
					label={"Amount to share:"}
					min='0'
					require={true}
				/>
				{/* <div>
				<label htmlFor='share-amount'>Amount to share:</label>
				<input
					type='number'
					id='share-amount'
					ref={amountShareInputRef}
					min='0'
				/>
			</div> */}

				<button
					onClick={clickHandler}
					className='focus:outline-none text-white bg-green-500 hover:bg-green-400 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800'
				>
					Add person share
				</button>
			</form>
		</div>
	);
};

export default AdvanceForm;
