import React, { useState } from "react";
import BasicForm from "./BasicForm";
import BasicTable from "./BasicTable";
import ScreenSplit from "../../../../util/ScreenSplit";

const BasicSplitter: React.FC = () => {
	const [output, setOutput] = useState<{}[]>([
		{ "Amount Split:": 0 },
		{ "no. of Person/s:": 0 },
		{ "Total Bill:": 0 },
	]);

	const calculateHandler = (amountInput: number, personInput: number) => {
		const calculatedAmount = +(amountInput / personInput).toFixed(2);

		setOutput([
			{ "Amount Split:": calculatedAmount },
			{ "no. of Person/s:": personInput },
			{ "Total Bill:": amountInput },
		]);
	};

	return (
		<div className='mx-4 flex flex-col items-center mt-2 sm:grid sm:grid-cols-2 sm:gap-8 sm:mt-8 lg:gap-16'>
			<BasicForm onCalculate={calculateHandler} />
			<BasicTable output={output} />
		</div>
	);
};

export default BasicSplitter;
