import React, { useRef } from "react";

interface PropsType {
	onCalculate: (amount: number, person: number) => void;
}

const BasicForm: React.FC<PropsType> = (props) => {
	const amountInputRef = useRef<HTMLInputElement>(null);
	const personInputRef = useRef<HTMLInputElement>(null);
	const submitHandler = (e: React.FormEvent) => {
		e.preventDefault();
		const amountInput = +amountInputRef.current!.value;
		const personInput = +personInputRef.current!.value;
		if (!amountInput || amountInput < 1 || !personInput || personInput < 1) {
			console.log("invalid");
			return;
		}
		props.onCalculate(amountInput, personInput);
	};
	return (
		<form
			onSubmit={submitHandler}
			className='w-full max-w-md bg-blue-300 border rounded-lg shadow-md justify-self-end p-4 mb-8 sm:mb-0 h-64'
		>
			<div className='mb-6'>
				<label
					htmlFor='amount'
					className='block mb-2 text-sm font-medium text-gray-900 dark:text-white'
				>
					Amount:
				</label>
				<input
					type='number'
					id='amount'
					className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
					// placeholder='John'
					ref={amountInputRef}
					min='1'
					required
				/>
			</div>
			<div className='mb-6'>
				<label
					htmlFor='person'
					className='block mb-2 text-sm font-medium text-gray-900 dark:text-white'
				>
					How many Person/s:
				</label>
				<input
					type='number'
					id='person'
					className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 w-full  p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500'
					// placeholder='John'
					ref={personInputRef}
					min='1'
					required
				/>
			</div>
			<button
				type='submit'
				className='focus:outline-none text-white bg-green-500 hover:bg-green-400 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800'
			>
				SPLIT BILL
			</button>
		</form>
	);
};

export default BasicForm;
