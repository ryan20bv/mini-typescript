import React from "react";

interface PropsType {
	output: {}[];
}

const BasicTable: React.FC<PropsType> = ({ output }) => {
	return (
		<div className='w-full max-w-md p-8 bg-green-300 border rounded-lg shadow-md sm:h-64'>
			<div className='flow-root'>
				<ul className='divide-y divide-gray-200 '>
					{output.map((item, index) => (
						<li className='py-3 sm:py-4' key={index}>
							<div className='flex items-center space-x-4'>
								<div className='flex-1 min-w-0'>
									<p className='text-gray-900'>{Object.keys(item)}</p>
								</div>
								<div className='inline-flex items-center text-base font-semibold text-gray-900'>
									{Object.values(item)}
								</div>
							</div>
						</li>
					))}
				</ul>
			</div>
		</div>
	);
};

export default BasicTable;
