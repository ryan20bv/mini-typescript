import React, { useEffect, useState } from "react";

import MovieList from "./movies/MovieList";

import { MovieModel } from "./movies/model/MovieModel";
import Modal from "../modal/Modal";
import ModalContent from "../modal/ModalContent";
import MovieCarousel from "../../util/MovieCarousel";
import LoadingUtil from "../../util/Loading";

const Movie = () => {
	const [movieList, setMovieList] = useState<MovieModel[]>([]);
	const [allotedMovies, setAllotedMovies] = useState<MovieModel[][]>([]);
	const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
	const [selectedMovie, setSelectedMovie] = useState<MovieModel>({
		id: "",
		embed_url: "",
		title: "",
		thumbnail: "",
		youtube_id: "",
	});
	const [randomMovie, setRandomMovie] = useState<MovieModel[]>([]);
	const [isLoading, setIsLoading] = useState<boolean>(true);

	const allotMoviesHandler = (movieList: MovieModel[]) => {
		const copyOfMovieList = movieList;
		const newArray: MovieModel[][] = [];

		const recursionFn = (movieArray: MovieModel[]) => {
			if (movieArray.length > 10) {
				newArray.push(movieArray.slice(0, 10));
				const newData: MovieModel[] = movieArray.slice(10);
				if (newData.length > 0) {
					recursionFn(newData);
				}
			} else {
				newArray.push(movieArray);
			}
		};
		recursionFn(copyOfMovieList);

		setAllotedMovies(newArray);
	};
	useEffect(() => {
		if (movieList.length > 0 && !isLoading) {
			let contentLength = 10;
			let movieArrayLength = movieList.length;
			let idArray: number[] = [];
			for (let i = 0; i < contentLength; i++) {
				let randomNumber: number;
				const rollRandomFN = () => {
					randomNumber = Math.floor(Math.random() * movieArrayLength);
					if (idArray.includes(randomNumber)) {
						rollRandomFN();
					}
					idArray.push(randomNumber);
				};
				rollRandomFN();

				let foundMovie = movieList.find((item) => {
					return +item.id === randomNumber;
				});
				if (foundMovie === undefined) {
					throw new TypeError("The value was promised to always be there!");
				}
				setRandomMovie((prevMovie) => {
					return prevMovie.concat(foundMovie!);
				});
			}
		}
	}, [movieList, isLoading]);

	useEffect(() => {
		const movieStorage = localStorage.getItem("movieList");
		let storedMovie: MovieModel[] = [];
		if (typeof movieStorage === "string") {
			storedMovie = JSON.parse(movieStorage);
			if (storedMovie.length > 0) {
				setMovieList([...storedMovie]);
				allotMoviesHandler(storedMovie);
				setIsLoading(false);
				return;
			}
		}

		const fetchMovieList = async () => {
			const options = {
				method: "GET",
				headers: {
					"X-RapidAPI-Key":
						"1aa819fccamsh9e6a2a9746f29cbp1f7f19jsn6511619a612a",
					"X-RapidAPI-Host":
						"new-movie-trailers-api-daily-update-best-upcoming-movies.p.rapidapi.com",
				},
			};
			try {
				const response = await fetch(
					"https://new-movie-trailers-api-daily-update-best-upcoming-movies.p.rapidapi.com/",
					options
				);
				if (!response.ok) {
					throw new Error("Error");
				}
				const data: MovieModel[] = await response.json();
				setMovieList([...data]);
				allotMoviesHandler(data);
				localStorage.setItem("movieList", JSON.stringify(data));
				setIsLoading(false);
			} catch (err) {
				console.log(err);
			}
		};

		fetchMovieList();
	}, []);

	const closeModalHandler = () => {
		setIsModalOpen(false);
	};
	const openModalHandler = (id: string) => {
		setIsModalOpen(true);

		const foundMovie = movieList.find((item) => item.id === id);

		if (foundMovie === undefined) {
			throw new TypeError("The value promised is undefined");
		}
		setSelectedMovie(foundMovie);
	};

	return (
		<div className='pt-20 bg-black text-white text-lg min-h-screen'>
			{isLoading && <LoadingUtil />}
			{isModalOpen && (
				<Modal onClose={closeModalHandler}>
					<ModalContent movie={selectedMovie!} />
				</Modal>
			)}
			{randomMovie && randomMovie.length > 0 && (
				<MovieCarousel
					initialMovie={randomMovie}
					onOpenModal={openModalHandler}
				/>
			)}
			{!isLoading && allotedMovies.length > 0 && (
				<div className='flex flex-col overflow-x-hidden '>
					{allotedMovies.map((movies, index) => (
						<MovieList
							key={index}
							movies={movies}
							index={index}
							onOpenModal={openModalHandler}
						/>
					))}
				</div>
			)}
		</div>
	);
};

export default Movie;
