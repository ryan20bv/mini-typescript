import React, { useState, useEffect } from "react";
import { NavLink, Outlet, useNavigate } from "react-router-dom";

const activeNav =
	"inline-block p-4 text-blue-600 border-b-2 border-blue-600 rounded-t-lg active dark:text-blue-500 dark:border-blue-500 mx-10";
const unActiveNav =
	"inline-block p-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300 mx-10";

const BillSplitter = () => {
	const navigate = useNavigate();
	const [idNavigate, setIdNavigate] = useState<string>("basic");
	useEffect(() => {
		navigate(`/bill-splitter/${idNavigate}`);
	}, [idNavigate, navigate]);
	const testFunction = (id: string) => {
		setIdNavigate(id);
	};
	return (
		<div>
			<div className='pt-14 px-4 flex flex-col items-center md:pt-20'>
				<h1 className='text-3xl font-extrabold dark:text-white'>
					Bill Splitter
				</h1>

				<div className='text-sm font-medium text-center text-gray-500 border-b border-gray-200 '>
					<ul className='flex flex-wrap mb-2 md:mb-8'>
						<NavLink
							// className='mr-2'
							to='/bill-splitter/basic'
							className={(navData) =>
								navData.isActive ? activeNav : unActiveNav
							}
							onClick={() => testFunction("basic")}
							aria-current='page'
						>
							BASIC
						</NavLink>
						<NavLink
							to='/bill-splitter/advance'
							className={(navData) =>
								navData.isActive ? activeNav : unActiveNav
							}
							// aria-current='page'
							onClick={() => testFunction("advance")}
						>
							ADVANCE
						</NavLink>
					</ul>
				</div>
			</div>
			<Outlet />
		</div>
	);
};
export default BillSplitter;
