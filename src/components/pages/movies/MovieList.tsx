import React, { useRef, useEffect } from "react";
import MovieCardUtil from "../../../util/MovieCardUtil";
import { MovieModel } from "./model/MovieModel";

interface PropsType {
	movies: MovieModel[];
	index: number;
	onOpenModal: (id: string) => void;
}

const movieCategory: string[] = [
	"Movie in Demand",
	"Thriller",
	"Comedy",
	"K-Drama",
	"English Movies",
	"Action",
	"Suspense",
	"For Kids",
	"Cartoons",
];

const MovieList: React.FC<PropsType> = (props) => {
	const cardDivRef = useRef<HTMLDivElement>(null);
	useEffect(() => {
		cardDivRef.current!.scrollLeft = 0;
	}, [cardDivRef]);

	const scrollRightHandler = () => {
		cardDivRef.current!.scrollLeft += 200;
	};
	const scrollLeftHandler = () => {
		if (cardDivRef.current!.scrollLeft < 0) {
			return;
		}
		cardDivRef.current!.scrollLeft -= 200;
	};

	return (
		<div className=' my-2 lg:mx-4'>
			<h1 className='ml-4 text-2xl font-medium'>
				{movieCategory[props.index]}
			</h1>
			<div className='flex items-center justify-between '>
				<div
					className='hidden lg:block lg:m-2 hover:cursor-pointer '
					onClick={scrollLeftHandler}
				>
					<h1 className='text-4xl'> {"<"} </h1>
				</div>
				<div
					className='flex flex-col overflow-x-scroll xl:overflow-x-hidden '
					ref={cardDivRef}
				>
					<div className='flex flex-rol  w-max '>
						{props.movies.map((item) => {
							const { id, title, thumbnail, embed_url, youtube_id } = item;
							return (
								<MovieCardUtil
									key={id}
									id={id}
									title={title}
									thumbnail={thumbnail}
									embed_url={embed_url}
									youtube_id={youtube_id}
									onOpenModal={props.onOpenModal.bind(null, id)}
								/>
							);
						})}
					</div>
				</div>
				<div
					className='hidden lg:block lg:m-2 hover:cursor-pointer'
					onClick={scrollRightHandler}
				>
					<h1 className='text-4xl'> {">"} </h1>
				</div>
			</div>
		</div>
	);
};

export default MovieList;
