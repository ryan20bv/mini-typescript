export class MovieModel {
	// embed_url: string;
	// id: string;
	// thumbnail: string;
	// title: string;
	// youtube_id: string;
	constructor(
		public embed_url: string,
		public id: string,
		public thumbnail: string,
		public title: string,
		public youtube_id: string
	) {}
}
